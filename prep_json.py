import json

def make_func_name():
    with open('fswcommands.json', 'r') as f:
        instr = json.load(f)

    for f in instr['functions']:
        tmp = f['commands'][0]
        f['name'] = tmp.split(':', maxsplit=1)[1].split('?')[0].lower().replace(':', '_')

    with open('fswcommands.json', 'w') as f:
        json.dump(instr, fp=f, indent=2)
    
def add_type_command():
    with open('fswcommands.json', 'r') as f:
        instr = json.load(f)

    for f in instr['functions']:
        for i, c in enumerate(f['commands']):
            f['commands'][i] = {'command': c, 'type': 'float'}
            

    with open('fswcommands.json', 'w') as f:
        json.dump(instr, fp=f, indent=2)

def add_var():
    with open('fswcommands.json', 'r') as f:
        instr = json.load(f)

    for f in instr['functions']:
        vars = ['curr', 'avg', 'max', 'min']
        for i, (v, c) in enumerate(zip(vars, f['commands'])):
            f['commands'][i]['var'] = v
            

    with open('fswcommands.json', 'w') as f:
        json.dump(instr, fp=f, indent=2)

def add_return():
    with open('fswcommands.json', 'r') as f:
        instr = json.load(f)

    for f in instr['functions']:
        f['ret'] = {'val': 'SummaryValues(curr, avg, max, min)', 
            'type': 'SummaryValues'}
            
    with open('fswcommands.json', 'w') as f:
        json.dump(instr, fp=f, indent=2)

add_return()