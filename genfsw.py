from jinja2 import Environment, FileSystemLoader
import json

env = Environment(loader=FileSystemLoader("templates/"))
template = env.get_template("class.txt")
with open('fswcommands.json', 'r') as f:
    instr = json.load(f)

for func in instr['functions']:
    for i, comm in enumerate(func['commands']):
        for k, v in func['swap'].items():
            func['commands'][i]['command'] = comm['command'].replace(k, v)

content = template.render(obj_type=instr['obj_type'], name=instr['name'], base_class=instr['base_class'], 
    functions = instr['functions'])
with open('fsw.py', 'w') as f:
    f.write(content)
