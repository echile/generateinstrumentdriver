from typing import NamedTuple

class SummaryValues(NamedTuple):
    last: float
    average: float
    maximum: float
    minimum: float

class SigGen():

    def summary_evm_phr_level(self, window:int) -> SummaryValues:
    
        curr = float(self.query(f'FETCh{window}:SUMMary:EVM:PHR:LEVel?)', '\n'))
    
        avg = float(self.query(f'FETCh{window}:SUMMary:EVM:PHR:LEVel:AVERage?)', '\n'))
    
        max = float(self.query(f'FETCh{window}:SUMMary:EVM:PHR:LEVel:MAXimum?)', '\n'))
    
        min = float(self.query(f'FETCh{window}:SUMMary:EVM:PHR:LEVel:MINimum?)', '\n'))
    
        return SummaryValues(curr, avg, max, min)

    def summary_evm_phr_nrmse(self, window:int) -> SummaryValues:
    
        curr = float(self.query(f'FETCh{window}:SUMMary:EVM:PHR:NRMSe?)', '\n'))
    
        avg = float(self.query(f'FETCh{window}:SUMMary:EVM:PHR:NRMSe:AVERage?)', '\n'))
    
        max = float(self.query(f'FETCh{window}:SUMMary:EVM:PHR:NRMSe:MAXimum?)', '\n'))
    
        min = float(self.query(f'FETCh{window}:SUMMary:EVM:PHR:NRMSe:MINimum?)', '\n'))
    
        return SummaryValues(curr, avg, max, min)

    def summary_evm_psdu_level(self, window:int) -> SummaryValues:
    
        curr = float(self.query(f'FETCh{window}:SUMMary:EVM:PSDU:LEVel?)', '\n'))
    
        avg = float(self.query(f'FETCh{window}:SUMMary:EVM:PSDU:LEVel:AVERage?)', '\n'))
    
        max = float(self.query(f'FETCh{window}:SUMMary:EVM:PSDU:LEVel:MAXimum?)', '\n'))
    
        min = float(self.query(f'FETCh{window}:SUMMary:EVM:PSDU:LEVel:MINimum?)', '\n'))
    
        return SummaryValues(curr, avg, max, min)

    def summary_evm_psdu_nrmse(self, window:int) -> SummaryValues:
    
        curr = float(self.query(f'FETCh{window}:SUMMary:EVM:PSDU:NRMSe?)', '\n'))
    
        avg = float(self.query(f'FETCh{window}:SUMMary:EVM:PSDU:NRMSe:AVERage?)', '\n'))
    
        max = float(self.query(f'FETCh{window}:SUMMary:EVM:PSDU:NRMSe:MAXimum?)', '\n'))
    
        min = float(self.query(f'FETCh{window}:SUMMary:EVM:PSDU:NRMSe:MINimum?)', '\n'))
    
        return SummaryValues(curr, avg, max, min)

    def summary_evm_shr_nrmse(self, window:int) -> SummaryValues:
    
        curr = float(self.query(f'FETCh{window}:SUMMary:EVM:SHR:NRMSe?)', '\n'))
    
        avg = float(self.query(f'FETCh{window}:SUMMary:EVM:SHR:NRMSe:AVERage?)', '\n'))
    
        max = float(self.query(f'FETCh{window}:SUMMary:EVM:SHR:NRMSe:MAXimum?)', '\n'))
    
        min = float(self.query(f'FETCh{window}:SUMMary:EVM:SHR:NRMSe:MINimum?)', '\n'))
    
        return SummaryValues(curr, avg, max, min)

    def summary_evm_sts_level(self, window:int) -> SummaryValues:
    
        curr = float(self.query(f'FETCh{window}:SUMMary:EVM:STS:LEVel?)', '\n'))
    
        avg = float(self.query(f'FETCh{window}:SUMMary:EVM:STS:LEVel:AVERage?)', '\n'))
    
        max = float(self.query(f'FETCh{window}:SUMMary:EVM:STS:LEVel:MAXimum?)', '\n'))
    
        min = float(self.query(f'FETCh{window}:SUMMary:EVM:STS:LEVel:MINimum?)', '\n'))
    
        return SummaryValues(curr, avg, max, min)

    def summary_evm_sts_nrmse(self, window:int) -> SummaryValues:
    
        curr = float(self.query(f'FETCh{window}:SUMMary:EVM:STS:NRMSe?)', '\n'))
    
        avg = float(self.query(f'FETCh{window}:SUMMary:EVM:STS:NRMSe:AVERage?)', '\n'))
    
        max = float(self.query(f'FETCh{window}:SUMMary:EVM:STS:NRMSe:MAXimum?)', '\n'))
    
        min = float(self.query(f'FETCh{window}:SUMMary:EVM:STS:NRMSe:MINimum?)', '\n'))
    
        return SummaryValues(curr, avg, max, min)

    def summary_frequency_chip_error(self, window:int) -> SummaryValues:
    
        curr = float(self.query(f'FETCh{window}:SUMMary:FREQuency:CHIP:ERRor?)', '\n'))
    
        avg = float(self.query(f'FETCh{window}:SUMMary:FREQuency:CHIP:ERRor:AVERage?)', '\n'))
    
        max = float(self.query(f'FETCh{window}:SUMMary:FREQuency:CHIP:ERRor:MAXimum?)', '\n'))
    
        min = float(self.query(f'FETCh{window}:SUMMary:FREQuency:CHIP:ERRor:MINimum?)', '\n'))
    
        return SummaryValues(curr, avg, max, min)

    def summary_frequency_offset_hz(self, window:int) -> SummaryValues:
    
        curr = float(self.query(f'FETCh{window}:SUMMary:FREQuency:OFFSet:HZ?)', '\n'))
    
        avg = float(self.query(f'FETCh{window}:SUMMary:FREQuency:OFFSet:HZ:AVERage?)', '\n'))
    
        max = float(self.query(f'FETCh{window}:SUMMary:FREQuency:OFFSet:HZ:MAXimum?)', '\n'))
    
        min = float(self.query(f'FETCh{window}:SUMMary:FREQuency:OFFSet:HZ:MINimum?)', '\n'))
    
        return SummaryValues(curr, avg, max, min)

    def summary_frequency_offset_ppm(self, window:int) -> SummaryValues:
    
        curr = float(self.query(f'FETCh{window}:SUMMary:FREQuency:OFFSet:PPM?)', '\n'))
    
        avg = float(self.query(f'FETCh{window}:SUMMary:FREQuency:OFFSet:PPM:AVERage?)', '\n'))
    
        max = float(self.query(f'FETCh{window}:SUMMary:FREQuency:OFFSet:PPM:MAXimum?)', '\n'))
    
        min = float(self.query(f'FETCh{window}:SUMMary:FREQuency:OFFSet:PPM:MINimum?)', '\n'))
    
        return SummaryValues(curr, avg, max, min)

    def summary_jitter_chip(self, window:int) -> SummaryValues:
    
        curr = float(self.query(f'FETCh{window}:SUMMary:JITTer:CHIP?)', '\n'))
    
        avg = float(self.query(f'FETCh{window}:SUMMary:JITTer:CHIP:AVERage?)', '\n'))
    
        max = float(self.query(f'FETCh{window}:SUMMary:JITTer:CHIP:MAXimum?)', '\n'))
    
        min = float(self.query(f'FETCh{window}:SUMMary:JITTer:CHIP:MINimum?)', '\n'))
    
        return SummaryValues(curr, avg, max, min)

    def summary_jitter_symbol(self, window:int) -> SummaryValues:
    
        curr = float(self.query(f'FETCh{window}:SUMMary:JITTer:SYMBol?)', '\n'))
    
        avg = float(self.query(f'FETCh{window}:SUMMary:JITTer:SYMBol:AVERage?)', '\n'))
    
        max = float(self.query(f'FETCh{window}:SUMMary:JITTer:SYMBol:MAXimum?)', '\n'))
    
        min = float(self.query(f'FETCh{window}:SUMMary:JITTer:SYMBol:MINimum?)', '\n'))
    
        return SummaryValues(curr, avg, max, min)

    def summary_power_psdu_mean(self, window:int) -> SummaryValues:
    
        curr = float(self.query(f'FETCh{window}:SUMMary:POWer:PSDU:MEAN?)', '\n'))
    
        avg = float(self.query(f'FETCh{window}:SUMMary:POWer:PSDU:MEAN:AVERage?)', '\n'))
    
        max = float(self.query(f'FETCh{window}:SUMMary:POWer:PSDU:MEAN:MAXimum?)', '\n'))
    
        min = float(self.query(f'FETCh{window}:SUMMary:POWer:PSDU:MEAN:MINimum?)', '\n'))
    
        return SummaryValues(curr, avg, max, min)

    def summary_power_psdu_peak(self, window:int) -> SummaryValues:
    
        curr = float(self.query(f'FETCh{window}:SUMMary:POWer:PSDU:PEAK?)', '\n'))
    
        avg = float(self.query(f'FETCh{window}:SUMMary:POWer:PSDU:PEAK:AVERage?)', '\n'))
    
        max = float(self.query(f'FETCh{window}:SUMMary:POWer:PSDU:PEAK:MAXimum?)', '\n'))
    
        min = float(self.query(f'FETCh{window}:SUMMary:POWer:PSDU:PEAK:MINimum?)', '\n'))
    
        return SummaryValues(curr, avg, max, min)

    def summary_power_shr_mean(self, window:int) -> SummaryValues:
    
        curr = float(self.query(f'FETCh{window}:SUMMary:POWer:SHR:MEAN?)', '\n'))
    
        avg = float(self.query(f'FETCh{window}:SUMMary:POWer:SHR:MEAN:AVERage?)', '\n'))
    
        max = float(self.query(f'FETCh{window}:SUMMary:POWer:SHR:MEAN:MAXimum?)', '\n'))
    
        min = float(self.query(f'FETCh{window}:SUMMary:POWer:SHR:MEAN:MINimum?)', '\n'))
    
        return SummaryValues(curr, avg, max, min)

    def summary_power_shr_peak(self, window:int) -> SummaryValues:
    
        curr = float(self.query(f'FETCh{window}:SUMMary:POWer:SHR:PEAK?)', '\n'))
    
        avg = float(self.query(f'FETCh{window}:SUMMary:POWer:SHR:PEAK:AVERage?)', '\n'))
    
        max = float(self.query(f'FETCh{window}:SUMMary:POWer:SHR:PEAK:MAXimum?)', '\n'))
    
        min = float(self.query(f'FETCh{window}:SUMMary:POWer:SHR:PEAK:MINimum?)', '\n'))
    
        return SummaryValues(curr, avg, max, min)

    def summary_power_packet_mean(self, window:int) -> SummaryValues:
    
        curr = float(self.query(f'FETCh{window}:SUMMary:POWer:PACKet:MEAN?)', '\n'))
    
        avg = float(self.query(f'FETCh{window}:SUMMary:POWer:PACKet:MEAN:AVERage?)', '\n'))
    
        max = float(self.query(f'FETCh{window}:SUMMary:POWer:PACKet:MEAN:MAXimum?)', '\n'))
    
        min = float(self.query(f'FETCh{window}:SUMMary:POWer:PACKet:MEAN:MINimum?)', '\n'))
    
        return SummaryValues(curr, avg, max, min)

    def summary_power_packet_peak(self, window:int) -> SummaryValues:
    
        curr = float(self.query(f'FETCh{window}:SUMMary:POWer:PACKet:PEAK?)', '\n'))
    
        avg = float(self.query(f'FETCh{window}:SUMMary:POWer:PACKet:PEAK:AVERage?)', '\n'))
    
        max = float(self.query(f'FETCh{window}:SUMMary:POWer:PACKet:PEAK:MAXimum?)', '\n'))
    
        min = float(self.query(f'FETCh{window}:SUMMary:POWer:PACKet:PEAK:MINimum?)', '\n'))
    
        return SummaryValues(curr, avg, max, min)

    def summary_power_iqoffset(self, window:int) -> SummaryValues:
    
        curr = float(self.query(f'FETCh{window}:SUMMary:POWer:IQOFfset?)', '\n'))
    
        avg = float(self.query(f'FETCh{window}:SUMMary:POWer:IQOFfset:AVERage?)', '\n'))
    
        max = float(self.query(f'FETCh{window}:SUMMary:POWer:IQOFfset:MAXimum?)', '\n'))
    
        min = float(self.query(f'FETCh{window}:SUMMary:POWer:IQOFfset:MINimum?)', '\n'))
    
        return SummaryValues(curr, avg, max, min)

    def summary_pulse_mask_passed(self, window:int) -> SummaryValues:
    
        curr = float(self.query(f'FETCh{window}:SUMMary:PULSe:MASK:PASSed?)', '\n'))
    
        avg = float(self.query(f'FETCh{window}:SUMMary:PULSe:MASK:PASSed:AVERage?)', '\n'))
    
        max = float(self.query(f'FETCh{window}:SUMMary:PULSe:MASK:PASSed:MAXimum?)', '\n'))
    
        min = float(self.query(f'FETCh{window}:SUMMary:PULSe:MASK:PASSed:MINimum?)', '\n'))
    
        return SummaryValues(curr, avg, max, min)

    def summary_pulse_rise_monotonic(self, window:int) -> SummaryValues:
    
        curr = float(self.query(f'FETCh{window}:SUMMary:PULSe:RISE:MONotonic?)', '\n'))
    
        avg = float(self.query(f'FETCh{window}:SUMMary:PULSe:RISE:MONotonic:AVERage?)', '\n'))
    
        max = float(self.query(f'FETCh{window}:SUMMary:PULSe:RISE:MONotonic:MAXimum?)', '\n'))
    
        min = float(self.query(f'FETCh{window}:SUMMary:PULSe:RISE:MONotonic:MINimum?)', '\n'))
    
        return SummaryValues(curr, avg, max, min)

    def summary_pulse_location_sync(self, window:int) -> SummaryValues:
    
        curr = float(self.query(f'FETCh{window}:SUMMary:PULSe:LOCation:SYNC?)', '\n'))
    
        avg = float(self.query(f'FETCh{window}:SUMMary:PULSe:LOCation:SYNC:AVERage?)', '\n'))
    
        max = float(self.query(f'FETCh{window}:SUMMary:PULSe:LOCation:SYNC:MAXimum?)', '\n'))
    
        min = float(self.query(f'FETCh{window}:SUMMary:PULSe:LOCation:SYNC:MINimum?)', '\n'))
    
        return SummaryValues(curr, avg, max, min)

    def summary_pulse_location_sfd(self, window:int) -> SummaryValues:
    
        curr = float(self.query(f'FETCh{window}:SUMMary:PULSe:LOCation:SFD?)', '\n'))
    
        avg = float(self.query(f'FETCh{window}:SUMMary:PULSe:LOCation:SFD:AVERage?)', '\n'))
    
        max = float(self.query(f'FETCh{window}:SUMMary:PULSe:LOCation:SFD:MAXimum?)', '\n'))
    
        min = float(self.query(f'FETCh{window}:SUMMary:PULSe:LOCation:SFD:MINimum?)', '\n'))
    
        return SummaryValues(curr, avg, max, min)

    def summary_pulse_location_sts(self, window:int) -> SummaryValues:
    
        curr = float(self.query(f'FETCh{window}:SUMMary:PULSe:LOCation:STS?)', '\n'))
    
        avg = float(self.query(f'FETCh{window}:SUMMary:PULSe:LOCation:STS:AVERage?)', '\n'))
    
        max = float(self.query(f'FETCh{window}:SUMMary:PULSe:LOCation:STS:MAXimum?)', '\n'))
    
        min = float(self.query(f'FETCh{window}:SUMMary:PULSe:LOCation:STS:MINimum?)', '\n'))
    
        return SummaryValues(curr, avg, max, min)

    def summary_spectrum_mask_passed(self, window:int) -> SummaryValues:
    
        curr = float(self.query(f'FETCh{window}:SUMMary:SPECtrum:MASK:PASSed?)', '\n'))
    
        avg = float(self.query(f'FETCh{window}:SUMMary:SPECtrum:MASK:PASSed:AVERage?)', '\n'))
    
        max = float(self.query(f'FETCh{window}:SUMMary:SPECtrum:MASK:PASSed:MAXimum?)', '\n'))
    
        min = float(self.query(f'FETCh{window}:SUMMary:SPECtrum:MASK:PASSed:MINimum?)', '\n'))
    
        return SummaryValues(curr, avg, max, min)

    def summary_spectrum_maximum_power(self, window:int) -> SummaryValues:
    
        curr = float(self.query(f'FETCh{window}:SUMMary:SPECtrum:MAXimum:POWer?)', '\n'))
    
        avg = float(self.query(f'FETCh{window}:SUMMary:SPECtrum:MAXimum:POWer:AVERage?)', '\n'))
    
        max = float(self.query(f'FETCh{window}:SUMMary:SPECtrum:MAXimum:POWer:MAXimum?)', '\n'))
    
        min = float(self.query(f'FETCh{window}:SUMMary:SPECtrum:MAXimum:POWer:MINimum?)', '\n'))
    
        return SummaryValues(curr, avg, max, min)

    def summary_xcorr_mlobe_minimum_width(self, window:int) -> SummaryValues:
    
        curr = float(self.query(f'FETCh{window}:SUMMary:XCORr:MLOBe:MINimum:WIDTh?)', '\n'))
    
        avg = float(self.query(f'FETCh{window}:SUMMary:XCORr:MLOBe:MINimum:WIDTh:AVERage?)', '\n'))
    
        max = float(self.query(f'FETCh{window}:SUMMary:XCORr:MLOBe:MINimum:WIDTh:MAXimum?)', '\n'))
    
        min = float(self.query(f'FETCh{window}:SUMMary:XCORr:MLOBe:MINimum:WIDTh:MINimum?)', '\n'))
    
        return SummaryValues(curr, avg, max, min)

    def summary_xcorr_mlobe_peak(self, window:int) -> SummaryValues:
    
        curr = float(self.query(f'FETCh{window}:SUMMary:XCORr:MLOBe:PEAK?)', '\n'))
    
        avg = float(self.query(f'FETCh{window}:SUMMary:XCORr:MLOBe:PEAK:AVERage?)', '\n'))
    
        max = float(self.query(f'FETCh{window}:SUMMary:XCORr:MLOBe:PEAK:MAXimum?)', '\n'))
    
        min = float(self.query(f'FETCh{window}:SUMMary:XCORr:MLOBe:PEAK:MINimum?)', '\n'))
    
        return SummaryValues(curr, avg, max, min)

    def summary_xcorr_mlobe_width(self, window:int) -> SummaryValues:
    
        curr = float(self.query(f'FETCh{window}:SUMMary:XCORr:MLOBe:WIDTh?)', '\n'))
    
        avg = float(self.query(f'FETCh{window}:SUMMary:XCORr:MLOBe:WIDTh:AVERage?)', '\n'))
    
        max = float(self.query(f'FETCh{window}:SUMMary:XCORr:MLOBe:WIDTh:MAXimum?)', '\n'))
    
        min = float(self.query(f'FETCh{window}:SUMMary:XCORr:MLOBe:WIDTh:MINimum?)', '\n'))
    
        return SummaryValues(curr, avg, max, min)

    def summary_xcorr_mlobe_width_passed(self, window:int) -> SummaryValues:
    
        curr = float(self.query(f'FETCh{window}:SUMMary:XCORr:MLOBe:WIDTh:PASSed?)', '\n'))
    
        avg = float(self.query(f'FETCh{window}:SUMMary:XCORr:MLOBe:WIDTh:PASSed:AVERage?)', '\n'))
    
        max = float(self.query(f'FETCh{window}:SUMMary:XCORr:MLOBe:WIDTh:PASSed:MAXimum?)', '\n'))
    
        min = float(self.query(f'FETCh{window}:SUMMary:XCORr:MLOBe:WIDTh:PASSed:MINimum?)', '\n'))
    
        return SummaryValues(curr, avg, max, min)

    def summary_xcorr_nmse(self, window:int) -> SummaryValues:
    
        curr = float(self.query(f'FETCh{window}:SUMMary:XCORr:NMSE?)', '\n'))
    
        avg = float(self.query(f'FETCh{window}:SUMMary:XCORr:NMSE:AVERage?)', '\n'))
    
        max = float(self.query(f'FETCh{window}:SUMMary:XCORr:NMSE:MAXimum?)', '\n'))
    
        min = float(self.query(f'FETCh{window}:SUMMary:XCORr:NMSE:MINimum?)', '\n'))
    
        return SummaryValues(curr, avg, max, min)

    def summary_xcorr_slobe_peak(self, window:int) -> SummaryValues:
    
        curr = float(self.query(f'FETCh{window}:SUMMary:XCORr:SLOBe:PEAK?)', '\n'))
    
        avg = float(self.query(f'FETCh{window}:SUMMary:XCORr:SLOBe:PEAK:AVERage?)', '\n'))
    
        max = float(self.query(f'FETCh{window}:SUMMary:XCORr:SLOBe:PEAK:MAXimum?)', '\n'))
    
        min = float(self.query(f'FETCh{window}:SUMMary:XCORr:SLOBe:PEAK:MINimum?)', '\n'))
    
        return SummaryValues(curr, avg, max, min)

    def summary_xcorr_slobe_peak_location(self, window:int) -> SummaryValues:
    
        curr = float(self.query(f'FETCh{window}:SUMMary:XCORr:SLOBe:PEAK:LOCation?)', '\n'))
    
        avg = float(self.query(f'FETCh{window}:SUMMary:XCORr:SLOBe:PEAK:LOCation:AVERage?)', '\n'))
    
        max = float(self.query(f'FETCh{window}:SUMMary:XCORr:SLOBe:PEAK:LOCation:MAXimum?)', '\n'))
    
        min = float(self.query(f'FETCh{window}:SUMMary:XCORr:SLOBe:PEAK:LOCation:MINimum?)', '\n'))
    
        return SummaryValues(curr, avg, max, min)

    def summary_xcorr_slobe_peak_passed(self, window:int) -> SummaryValues:
    
        curr = float(self.query(f'FETCh{window}:SUMMary:XCORr:SLOBe:PEAK:PASSed?)', '\n'))
    
        avg = float(self.query(f'FETCh{window}:SUMMary:XCORr:SLOBe:PEAK:PASSed:AVERage?)', '\n'))
    
        max = float(self.query(f'FETCh{window}:SUMMary:XCORr:SLOBe:PEAK:PASSed:MAXimum?)', '\n'))
    
        min = float(self.query(f'FETCh{window}:SUMMary:XCORr:SLOBe:PEAK:PASSed:MINimum?)', '\n'))
    
        return SummaryValues(curr, avg, max, min)

    def summary_power_sts_mean(self, window:int) -> SummaryValues:
    
        curr = float(self.query(f'FETCh{window}:SUMMary:POWer:STS:MEAN?)', '\n'))
    
        avg = float(self.query(f'FETCh{window}:SUMMary:POWer:STS:MEAN:AVERage?)', '\n'))
    
        max = float(self.query(f'FETCh{window}:SUMMary:POWer:STS:MEAN:MAXimum?)', '\n'))
    
        min = float(self.query(f'FETCh{window}:SUMMary:POWer:STS:MEAN:MINimum?)', '\n'))
    
        return SummaryValues(curr, avg, max, min)

    def summary_power_sts_peak(self, window:int) -> SummaryValues:
    
        curr = float(self.query(f'FETCh{window}:SUMMary:POWer:STS:PEAK?)', '\n'))
    
        avg = float(self.query(f'FETCh{window}:SUMMary:POWer:STS:PEAK:AVERage?)', '\n'))
    
        max = float(self.query(f'FETCh{window}:SUMMary:POWer:STS:PEAK:MAXimum?)', '\n'))
    
        min = float(self.query(f'FETCh{window}:SUMMary:POWer:STS:PEAK:MINimum?)', '\n'))
    
        return SummaryValues(curr, avg, max, min)

    def summary_pulse_rise_time(self, window:int) -> SummaryValues:
    
        curr = float(self.query(f'FETCh{window}:SUMMary:PULSe:RISE:TIME?)', '\n'))
    
        avg = float(self.query(f'FETCh{window}:SUMMary:PULSe:RISE:TIME:AVERage?)', '\n'))
    
        max = float(self.query(f'FETCh{window}:SUMMary:PULSe:RISE:TIME:MAXimum?)', '\n'))
    
        min = float(self.query(f'FETCh{window}:SUMMary:PULSe:RISE:TIME:MINimum?)', '\n'))
    
        return SummaryValues(curr, avg, max, min)

    def summary_pulse_rise_time_passed(self, window:int) -> SummaryValues:
    
        curr = float(self.query(f'FETCh{window}:SUMMary:PULSe:RISE:TIME:PASSed?)', '\n'))
    
        avg = float(self.query(f'FETCh{window}:SUMMary:PULSe:RISE:TIME:PASSed:AVERage?)', '\n'))
    
        max = float(self.query(f'FETCh{window}:SUMMary:PULSe:RISE:TIME:PASSed:MAXimum?)', '\n'))
    
        min = float(self.query(f'FETCh{window}:SUMMary:PULSe:RISE:TIME:PASSed:MINimum?)', '\n'))
    
        return SummaryValues(curr, avg, max, min)
